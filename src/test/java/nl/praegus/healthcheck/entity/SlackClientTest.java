package nl.praegus.healthcheck.entity;

import nl.praegus.healthcheck.SlackClient;
import nl.praegus.healthcheck.SlackException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.internal.verification.VerificationModeFactory.times;

@RunWith(MockitoJUnitRunner.class)
public class SlackClientTest {

    @Mock
    RestTemplate restTemplate;

    private SlackClient slackClient;

    @Before
    public void before() {
        slackClient = new SlackClient("channel", restTemplate, "http://localhost:8080/health");
    }

    @Test
    public void send_slack_message_went_ok() {
        slackClient.sendSlackMessage("test");

        verify(restTemplate, times(1)).postForObject(
                eq("http://localhost:8080/health"),
                eq(new Message("channel", "test")),
                eq(String.class));
    }

    @Test
    public void send_slack_message_for_multiple_unhealthy_services() {
        slackClient.sendSlackMessage(asList("service 1", "service 2"), HealthStatus.NOT_HEALTHY);

        verify(restTemplate, times(1)).postForObject(
                eq("http://localhost:8080/health"),
                eq(new Message("channel", "Services service 1, service 2 zijn *NIET* healthy! :cry:")),
                eq(String.class));
    }

    @Test
    public void send_slack_message_for_multiple_services_that_reagained_health() {
        slackClient.sendSlackMessage(asList("service 1", "service 2"), HealthStatus.REGAINED_HEALTH);

        verify(restTemplate, times(1)).postForObject(
                eq("http://localhost:8080/health"),
                eq(new Message("channel", "Services service 1, service 2 zijn weer healthy! :smile:")),
                eq(String.class));
    }

    @Test
    public void send_slack_message_exception_handling() {
        when(restTemplate.postForObject(anyString(), any(), any())).thenThrow(HttpClientErrorException.create(HttpStatus.BAD_REQUEST, "error", HttpHeaders.EMPTY, null, null));

        assertThatThrownBy(() -> slackClient.sendSlackMessage(asList("service 1", "service 2"), HealthStatus.REGAINED_HEALTH)).isInstanceOf(SlackException.class);
    }
}
