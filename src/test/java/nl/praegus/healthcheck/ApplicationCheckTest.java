package nl.praegus.healthcheck;

import nl.praegus.healthcheck.entity.HealthStatus;
import nl.praegus.healthcheck.entity.ServiceCheck;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Collections;

import static nl.praegus.healthcheck.entity.HealthStatus.ALREADY_HEALTHY;
import static nl.praegus.healthcheck.entity.HealthStatus.NOT_HEALTHY;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mockingDetails;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ApplicationCheckTest {

    @Mock
    private SlackClient slackClient;

    @Mock
    private ServiceCheck serviceCheck;

    @Test
    public void up_and_running() {
        when(serviceCheck.getName()).thenReturn("servicenaam");
        when(serviceCheck.getHealthStatus()).thenReturn(ALREADY_HEALTHY);

        ApplicationCheck applicationCheck = new ApplicationCheck(slackClient, Collections.singletonList(serviceCheck));

        applicationCheck.doContinuousApplicationHealthcheck(LocalDateTime.now().plusSeconds(1), LocalTime.now().plusMinutes(1), 1000, 2000);

        verify(slackClient, times(1)).sendSlackMessage("Healthcheck is up and running for services: servicenaam");
        verify(slackClient, times(1)).sendSlackMessage(any());
    }

    @Test
    public void up_and_running_and_unhealthy_service() {
        when(serviceCheck.getName()).thenReturn("servicenaam");
        when(serviceCheck.getHealthStatus()).thenReturn(NOT_HEALTHY);

        ApplicationCheck applicationCheck = new ApplicationCheck(slackClient, Collections.singletonList(serviceCheck));

        applicationCheck.doContinuousApplicationHealthcheck(LocalDateTime.now().plusSeconds(1), LocalTime.now().plusSeconds(10), 1000, 1000);

        verify(slackClient, times(1)).sendSlackMessage("Service servicenaam is unhealthy! :cry:");
    }

    @Test
    public void service_regains_health() {
        when(serviceCheck.getName()).thenReturn("servicenaam");
        when(serviceCheck.getHealthStatus()).thenReturn(HealthStatus.REGAINED_HEALTH);

        ApplicationCheck applicationCheck = new ApplicationCheck(slackClient, Collections.singletonList(serviceCheck));

        applicationCheck.doContinuousApplicationHealthcheck(LocalDateTime.now().plusSeconds(1), LocalTime.now().plusMinutes(10), 1000, 1000);

        verify(slackClient, times(1)).sendSlackMessage("Service servicenaam is weer healthy! :smile:");
    }

    @Test
    public void healthcheck_has_an_exception() {
        when(serviceCheck.getName()).thenReturn("servicenaam");
        when(serviceCheck.getHealthStatus()).thenThrow(new RuntimeException("exceptie!"));

        ApplicationCheck applicationCheck = new ApplicationCheck(slackClient, Collections.singletonList(serviceCheck));

        applicationCheck.doContinuousApplicationHealthcheck(LocalDateTime.now().plusSeconds(1), LocalTime.now().plusMinutes(10), 1000, 1000);

        verify(slackClient, times(1)).sendSlackMessage("<!channel> Healthchecker is interrupted!");
    }

    @Test
    public void hourly_check() {
        when(serviceCheck.getName()).thenReturn("servicenaam");
        when(serviceCheck.getHealthStatus()).thenReturn(ALREADY_HEALTHY);

        ApplicationCheck applicationCheck = new ApplicationCheck(slackClient, Collections.singletonList(serviceCheck));

        applicationCheck.doContinuousApplicationHealthcheck(LocalDateTime.now().plusSeconds(1), LocalTime.now().plusSeconds(1), 1000, 1000);

        System.out.println(mockingDetails(slackClient).printInvocations());
        verify(slackClient, times(1)).sendSlackMessage("Healthcheck is up and running for services: servicenaam");
    }
}