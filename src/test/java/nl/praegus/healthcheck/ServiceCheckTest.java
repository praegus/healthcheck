package nl.praegus.healthcheck;

import nl.praegus.healthcheck.entity.HealthStatus;
import nl.praegus.healthcheck.entity.ServiceCheck;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.internal.verification.VerificationModeFactory.times;
import static org.springframework.http.HttpStatus.OK;

@RunWith(MockitoJUnitRunner.class)
public class ServiceCheckTest {

    @Mock
    private TestRestTemplate restTemplate;

    @Test
    public void passed_healthcheck() {
        when(restTemplate.getForEntity(anyString(), any())).thenReturn(new ResponseEntity<>(OK));

        ServiceCheck serviceCheck = new ServiceCheck("name", "endpoint", restTemplate);

        serviceCheck.checkHealth();
        assertThat(serviceCheck.getHealthStatus()).isEqualTo(HealthStatus.ALREADY_HEALTHY);

        verify(restTemplate, times(1)).getForEntity("endpoint", String.class);
        assertThat(serviceCheck.getName()).isEqualTo("name");
    }

    @Test
    public void failed_healthcheck() {
        when(restTemplate.getForEntity(anyString(), any())).thenReturn(new ResponseEntity<>(HttpStatus.BAD_REQUEST));

        ServiceCheck serviceCheck = new ServiceCheck("name", "endpoint", restTemplate);

        serviceCheck.checkHealth();

        assertThat(serviceCheck.getHealthStatus()).isEqualTo(HealthStatus.NOT_HEALTHY);

        verify(restTemplate, times(1)).getForEntity("endpoint", String.class);
    }

    @Test
    public void failed_healthcheck_then_passed() {
        when(restTemplate.getForEntity(anyString(), any()))
                .thenReturn(new ResponseEntity<>(HttpStatus.BAD_REQUEST))
                .thenReturn(new ResponseEntity<>(OK));

        ServiceCheck serviceCheck = new ServiceCheck("name", "endpoint", restTemplate);

        serviceCheck.checkHealth();
        serviceCheck.checkHealth();

        assertThat(serviceCheck.getHealthStatus()).isEqualTo(HealthStatus.REGAINED_HEALTH);

        verify(restTemplate, times(2)).getForEntity("endpoint", String.class);
    }

    @Test
    public void healthcheck_throws_exception() {
        when(restTemplate.getForEntity(anyString(), any())).thenThrow(new RuntimeException("ecxeption"));

        ServiceCheck serviceCheck = new ServiceCheck("name", "endpoint", restTemplate);

        serviceCheck.checkHealth();

        assertThat(serviceCheck.getHealthStatus()).isEqualTo(HealthStatus.NOT_HEALTHY);

        verify(restTemplate, times(1)).getForEntity("endpoint", String.class);
    }
}