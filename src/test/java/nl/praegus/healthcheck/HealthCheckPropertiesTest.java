package nl.praegus.healthcheck;

import org.junit.Test;

import java.io.IOException;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class HealthCheckPropertiesTest {

    @Test
    public void no_property_file() {
        assertThatThrownBy(() -> new HealthcheckProperties(new String[]{"location"}))
                .isInstanceOf(IOException.class)
                .hasMessage("No property file found at location location");
    }

    @Test
    public void property_file_mist_service() {
        assertThatThrownBy(() -> new HealthcheckProperties(new String[]{"src/test/resources/geenservice.properties"}))
                .isInstanceOf(IOException.class)
                .hasMessage("Propertyfile mist service, webhook of channel property!");
    }

    @Test
    public void property_file_mist_channel() {
        assertThatThrownBy(() -> new HealthcheckProperties(new String[]{"src/test/resources/geenchannel.properties"}))
                .isInstanceOf(IOException.class)
                .hasMessage("Propertyfile mist service, webhook of channel property!");
    }

    @Test
    public void read_properties_uit_meerdere_bestanden() throws IOException {
        HealthcheckProperties properties = new HealthcheckProperties(
                new String[]{
                        "src/test/resources/service.properties",
                        "src/test/resources/geenservice.properties",
                        "src/test/resources/geenchannel.properties"});

        assertThat(properties.getChannel()).isEqualTo("postnl_test");
        assertThat(properties.getWebhook()).isEqualTo("www.webhook.com");
        assertThat(properties.getServices()).containsOnly(
                Map.entry("augmented", "http://augmented.com"),
                Map.entry("Report Portal api", "http://api:8080/api/v1/admin/health"));
    }
}
