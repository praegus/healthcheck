package nl.praegus.healthcheck;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.LocalTime;

public class HealthCheckApplication {

    public static void main(String[] args) throws IOException {
        HealthcheckProperties properties = new HealthcheckProperties(args);

        ApplicationCheck applicationHealthcheck = new ApplicationCheck(properties.getWebhook(), properties.getChannel(), properties.getServices());
        applicationHealthcheck.doContinuousApplicationHealthcheck(LocalDateTime.of(2030, 1, 1, 0, 0), LocalTime.of(0, 0), 60000, 120000);
    }
}
