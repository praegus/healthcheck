package nl.praegus.healthcheck;

import nl.praegus.healthcheck.entity.HealthStatus;
import nl.praegus.healthcheck.entity.ServiceCheck;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.OptionalDouble;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static java.lang.String.format;
import static java.util.stream.Collectors.toList;
import static nl.praegus.healthcheck.entity.HealthStatus.NOT_HEALTHY;
import static nl.praegus.healthcheck.entity.HealthStatus.REGAINED_HEALTH;

@SuppressWarnings("squid:S2189")
public class ApplicationCheck {
    private Logger logger = LoggerFactory.getLogger(ApplicationCheck.class);
    private List<ServiceCheck> services = new ArrayList<>();
    private SlackClient slackClient;

    public ApplicationCheck(String webhook, String channel, Map<String, String> services) {
        this.slackClient = new SlackClient(webhook, channel);
        mapServices(services);
    }

    public ApplicationCheck(SlackClient slackClient, List<ServiceCheck> services) {
        this.slackClient = slackClient;
        this.services = services;
    }

    public void doContinuousApplicationHealthcheck(LocalDateTime until, LocalTime timeToCheck, int sleep, int dailySleep) {
        slackClient.sendSlackMessage(format("Healthcheck is up and running for services: %s", getServiceNames()));
        try {
            while (until.isAfter(LocalDateTime.now())) {

                doServiceHealthChecks();
                Thread.sleep(sleep);

                LocalTime now = LocalTime.now();
                if (now.getHour() == timeToCheck.getHour() && now.getMinute() == timeToCheck.getMinute() && !isWeekend()) {
                    String message = format("Daily check: healthchecker is still running for services: %s", getServiceNames());
                    slackClient.sendSlackMessage(message);
                    Thread.sleep(dailySleep);
                }
            }
        } catch (Exception e) {
            slackClient.sendSlackMessage("<!channel> Healthchecker is interrupted!");
            logger.error("An exception is thrown! ", e);
        }
    }

    private boolean isWeekend() {
        return LocalDate.now().getDayOfWeek().equals(DayOfWeek.SATURDAY) || LocalDate.now().getDayOfWeek().equals(DayOfWeek.SUNDAY);
    }

    private String getServiceNames() {
        StringBuilder servicesString = new StringBuilder();
        for (int i = 0; i < services.size() - 1; i++) {
            servicesString.append(services.get(i).getName());
            servicesString.append(", ");
        }
        servicesString.append(services.get(services.size() - 1).getName());
        return servicesString.toString();
    }

    private void doServiceHealthChecks() {
        for (ServiceCheck serviceCheck : services) {
            serviceCheck.checkHealth();
        }
        report(NOT_HEALTHY, " is unhealthy! :cry:");
        report(REGAINED_HEALTH, " is weer healthy! :smile:");
    }

    private void report(HealthStatus healthStatus, String message) {
        List<ServiceCheck> servicesWithStatus = services.stream().filter(e -> e.getHealthStatus().equals(healthStatus)).collect(toList());
        OptionalDouble amountChecked = servicesWithStatus.stream().mapToDouble(service -> (double) service.getUnhealthyCount()).average();
        if (amountChecked.isPresent() && amountChecked.getAsDouble() < 5) {
            sendMessage(healthStatus, message, servicesWithStatus);
        } else if (amountChecked.isPresent() && (amountChecked.getAsDouble() % 60) == 0) {
            sendMessage(healthStatus, message, servicesWithStatus);
        }
    }

    private void sendMessage(HealthStatus healthStatus, String message, List<ServiceCheck> servicesWithStatus) {
        if (servicesWithStatus.size() == 1) {
            slackClient.sendSlackMessage("Service " + servicesWithStatus.get(0).getName() + message);
        } else if (servicesWithStatus.size() > 1) {
            slackClient.sendSlackMessage(servicesWithStatus.stream().map(ServiceCheck::getName).collect(toList()), healthStatus);
        }
    }

    private void mapServices(Map<String, String> services) {
        for (Map.Entry<String, String> service : services.entrySet()) {
            this.services.add(new ServiceCheck(service.getKey(), service.getValue()));
        }
    }
}
