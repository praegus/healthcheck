package nl.praegus.healthcheck;

import lombok.AllArgsConstructor;
import nl.praegus.healthcheck.entity.HealthStatus;
import nl.praegus.healthcheck.entity.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.List;

import static java.lang.String.format;

@AllArgsConstructor
public class SlackClient {
    private final String channel;
    private RestTemplate restTemplate;
    private String endpoint;
    private static Logger logger = LoggerFactory.getLogger(SlackClient.class);

    public SlackClient(String webhook, String channel) {
        this.endpoint = webhook;
        this.channel = channel;
        this.restTemplate = new RestTemplate();
    }

    public void sendSlackMessage(String message) {
        postMessage(message);
        logger.info("Slack message sent: {}", message);
    }

    public void sendSlackMessage(List<String> services, HealthStatus healthStatus) {
        StringBuilder message = new StringBuilder("Services ");
        for (int i = 0; i < services.size() - 1; i++) {
            message.append(services.get(i));
            message.append(", ");
        }
        message.append(services.get(services.size() - 1));
        if (healthStatus.equals(HealthStatus.NOT_HEALTHY)) {
            message.append(" zijn *NIET* healthy! :cry:");
        } else {
            message.append(" zijn weer healthy! :smile:");
        }

        postMessage(message.toString());
        logger.info("Slack message sent: {}", message);
    }

    private void postMessage(String message) {
        try {
            restTemplate.postForObject(endpoint, new Message(channel, message), String.class);
        } catch (HttpClientErrorException e) {
            throw new SlackException(format("Slack message is not sent, client error: Status: %s; Message: %s", e.getRawStatusCode(), e.getMessage()));
        } catch (Exception e) {
            throw new SlackException("Something went wrong in the communication with slack!", e);
        }
    }
}
