package nl.praegus.healthcheck.entity;

import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;

public class ServiceCheck {
    private String name;
    private String healthEndpoint;
    private HealthStatus healthStatus;
    private Integer unhealthyCount = 0;
    private TestRestTemplate restTemplate;

    public ServiceCheck(String name, String healthEndpoint) {
        this.name = name;
        this.healthEndpoint = healthEndpoint;
        this.restTemplate = new TestRestTemplate();
    }

    public ServiceCheck(String name, String healthEndpoint, TestRestTemplate restTemplate) {
        this.name = name;
        this.healthEndpoint = healthEndpoint;
        this.restTemplate = restTemplate;
    }

    public void checkHealth() {
        try {
            ResponseEntity<String> result = restTemplate.getForEntity(healthEndpoint, String.class);
            if (result.getStatusCode().is2xxSuccessful() && healthStatus != null && healthStatus.equals(HealthStatus.NOT_HEALTHY)) {
                healthStatus = HealthStatus.REGAINED_HEALTH;
                unhealthyCount = 0;
            } else if (result.getStatusCode().is2xxSuccessful()) {
                healthStatus = HealthStatus.ALREADY_HEALTHY;
                unhealthyCount = 0;
            } else if (healthStatus != null && healthStatus.equals(HealthStatus.NOT_HEALTHY)) {
                unhealthyCount++;
            } else {
                healthStatus = HealthStatus.NOT_HEALTHY;
                unhealthyCount = 1;
            }
        } catch (Exception e) {
            healthStatus = HealthStatus.NOT_HEALTHY;
        }
    }

    public String getName() {
        return name;
    }

    public HealthStatus getHealthStatus() {
        return healthStatus;
    }

    public int getUnhealthyCount() {
        return unhealthyCount;
    }
}
