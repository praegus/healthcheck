package nl.praegus.healthcheck.entity;

public enum HealthStatus {
    NOT_HEALTHY,
    STILL_UNHEALTHY,
    REGAINED_HEALTH,
    ALREADY_HEALTHY
}
