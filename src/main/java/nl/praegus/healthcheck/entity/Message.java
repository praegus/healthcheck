package nl.praegus.healthcheck.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Getter
@EqualsAndHashCode
public class Message {
    private String channel;
    private String username = "healthcheck";
    private String text;
    @JsonProperty("icon_emoji")
    private String iconEmoji = ":praegus:";

    public Message(String channel, String message) {
        this.channel = channel;
        this.text = message;
    }

}
