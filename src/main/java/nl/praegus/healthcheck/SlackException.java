package nl.praegus.healthcheck;

public class SlackException extends RuntimeException {
    public SlackException(String message) {
        super(message);
    }

    public SlackException(String message, Exception e) {
        super(message, e);
    }
}
