package nl.praegus.healthcheck;

import com.google.common.base.Splitter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import static java.lang.String.format;

public class HealthcheckProperties {
    private static Logger logger = LoggerFactory.getLogger(HealthcheckProperties.class);

    private String webhook;
    private String channel;
    private Map<String, String> services = new HashMap<>();

    public HealthcheckProperties(String[] fileLocations) throws IOException {
        for (String fileLocation : fileLocations) {
            Properties properties = readProperties(fileLocation);

            if (properties.containsKey("webhook")) {
                this.webhook = properties.getProperty("webhook");
            }
            if (properties.containsKey("channel")) {
                this.channel = properties.getProperty("channel");
            }
            if (properties.containsKey("services")) {
                this.services.putAll(parseMap(properties.getProperty("services")));
            }
        }
        if (webhook == null || channel == null || services.isEmpty()) {
            throw new IOException("Propertyfile mist service, webhook of channel property!");
        }
    }

    public Properties readProperties(String location) throws IOException {
        Properties properties = new Properties();
        try (InputStream input = new FileInputStream(location)) {
            properties.load(input);
        } catch (IOException e) {
            logger.error("No property file found at location {}", location, e);
            throw new IOException(format("No property file found at location %s", location));
        }
        return properties;
    }

    private static Map<String, String> parseMap(String formattedMap) {
        return Splitter.on(",").withKeyValueSeparator("=").split(formattedMap);
    }

    public String getWebhook() {
        return webhook;
    }

    public String getChannel() {
        return channel;
    }

    public Map<String, String> getServices() {
        return services;
    }
}
