pipeline {

    environment {
        GITLAB_USER = credentials("gitlab-macmini")
        GITLAB_ACCESS_TOKEN = credentials("gitlab-macmini-access-token")
    }

    tools {
        maven 'Maven3'
        jdk 'JDK8'
    }

    agent any

    stages {
        stage("Build docker container") {
            steps {
                script {
                    String artifact = downloadArtifact(env.projectId, env.jobId)
                    sh "unzip -o ${artifact}"
                    sh "docker build -t registry.gitlab.com/praegus/healthcheck:${version} ."
                    sh "echo $GITLAB_USER_PSW | docker login --username $GITLAB_USER_USR --password-stdin registry.gitlab.com"
                    sh "docker push registry.gitlab.com/praegus/healthcheck"
                    sh "rm /root/.docker/config.json"
                }
                slackSend(
                        channel: 'team-soju',
                        message: "Healthcheck ${params.version} succesfully released! :-)"
                )


            }
        }
    }
    post {
        unsuccessful {
            script {
                slackSend(
                        channel: 'team-soju',
                        message: "Healthcheck ${params.version} failed to be released :-("
                )
            }
        }
    }
}

String downloadArtifact(projectId, jobId) {
    try {
        sh "curl -L --fail --output artifacts.zip --header \"PRIVATE-TOKEN: ${GITLAB_ACCESS_TOKEN}\" \"https://gitlab.com/api/v4/projects/${projectId}/jobs/${jobId}/artifacts\""
        return "artifacts.zip"
    } catch (Exception e) {
        sh "echo ${e}"
        sleep 3
        return downloadArtifact(projectId, jobId)
    }
}