FROM openjdk:11.0.5-jre-slim
VOLUME /tmp
RUN apt update
RUN apt install -y curl
COPY target/healthcheck-*-with-dependencies.jar app.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar", "/config/generiek.properties","/config/omgeving.properties"]