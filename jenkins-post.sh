#!/usr/bin/env bash
projectId=$1
jobId=$2
version=$3
jenkinslogin=$4
jenkinsendpoint=$5

curl --show-error --fail -X POST "https://${jenkinslogin}@${jenkinsendpoint}/job/healthcheck-release/buildWithParameters?projectId=${projectId}&jobId=${jobId}&version=${version}"